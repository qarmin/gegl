### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.gnome.org/GNOME/gegl.git
cd gegl

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y gegl
apt install -y meson ninja-build

meson build/

ninja -C build/

```
### QTCreator Includes
```
/usr/include/glib-2.0/
.

```
